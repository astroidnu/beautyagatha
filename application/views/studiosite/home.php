	<!DOCTYPE html>
	<html lang="en">
	<head>
	<meta charset="UTF-8" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title><?php echo $title?></title>

	<!-- bootstrap -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css" />
	<!--my style-->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/style.css" />
	<!-- slides -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/da-slider/css/style2.css" />
	<!-- Jquery Load -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	</head>
	<body>
		<div class="container">
			<!-- Header Start -->
			<div class="col-md-12">
				<center><img src="<?php echo base_url()?>assets/images/logo/logo.png" width="280" height="180"></img></center>
					<!-- Sosial Media Icon -->
					<div class="sosialicon">
							<p align="center">
								<a href="#"><img src="<?php echo base_url()?>assets/images/iconsosmed/Facebook-icon.png"></img></a>
								<a href="#">
							<img src="<?php echo base_url()?>assets/images/iconsosmed/Google-Plus-icon.png"></img></a>
							<a href="#"><img src="<?php echo base_url()?>assets/images/iconsosmed/Instagram-icon.png" ></img></a>
							<a href="#"><img src="<?php echo base_url()?>assets/images/iconsosmed/Twitter-icon.png"></img></a>
							</p>
					</div>
					<!-- End Sosial Media Icon -->
			</div>
			<!-- Header end -->
			<!-- Menu Start -->
			<div class="col-md-12 col-sm-6 col-md-offset-0 col-sm-offset-3">
				<div class="menu-bar">
				    <ul class="nav navbar-nav">
				      <li class="dropdown">
				        <a href="#" data-toggle="collapse" data-target="#one">Home</a>
				      </li>
				      <li class="dropdown">
				        <a href="#" data-toggle="collapse" data-target="#two">Gallery</a>
				      </li>
				      <li class="dropdown">
				        <a href="#" data-toggle="collapse" data-target="#three">Package</a>
				      </li>
				        <li class="dropdown">
				        <a href="#" data-toggle="collapse" data-target="#three">About</a>
				      </li>
				         <li class="dropdown">
				        <a href="#" data-toggle="collapse" data-target="#three">Contact</a>
				      </li>
				    </ul>
				  </div>
			</div>
			<!-- menu End -->
			<!-- Home Page Slide Banner -->
			<div class="col-md-12">
				<div id="da-slider" class="da-slider">
					<div class="da-slide">
					<div class="da-img"><img src="<?php echo base_url()?>assets/images/banner/1.jpg" alt="image01" /></div>
				</div>
				<div class="da-slide">
					<div class="da-img"><img src="<?php echo base_url()?>assets/images/banner/2.jpg" alt="image01" /></div>
				</div>
				<div class="da-slide">
					<div class="da-img"><img src="<?php echo base_url()?>assets/images/banner/3.jpg" alt="image01" /></div>
				</div>
				<div class="da-slide">
					<div class="da-img"><img src="<?php echo base_url()?>assets/images/banner/4.jpg" alt="image01" /></div>
				</div>
				<nav class="da-arrows">
					<span class="da-arrows-prev"></span>
					<span class="da-arrows-next"></span>
				</nav>
				</div>
			</div>
			<!-- End Slide Banner -->
		</div>
	</body>
	<script>
	  $(function() {
				$('#da-slider').cslider({
					autoplay	: true,
					bgincrement	: 450
				});
	  });
	</script>
	<script type="text/javascript" data-cfasync="false">(function () { var done = false;var script = document.createElement("script");script.async = true;script.type = "text/javascript";script.src = "https://app.purechat.com/VisitorWidget/WidgetScript";document.getElementsByTagName("HEAD").item(0).appendChild(script);script.onreadystatechange = script.onload = function (e) {if (!done && (!this.readyState || this.readyState == "loaded" || this.readyState == "complete")) {var w = new PCWidget({ c: "00da54be-3be4-4d5d-bd21-45486178a414", f: true });done = true;}};})();</script>
	<!-- boostrap -->
	<script src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.js" type="text/javascript" ></script>
	<!-- slide -->
	<script src="<?php echo base_url()?>assets/plugins/da-slider/js/modernizr.custom.28468.js" type="text/javascript" ></script>
	<script src="<?php echo base_url()?>assets/plugins/da-slider/js/jquery.cslider.js" type="text/javascript" ></script>
	</html>
