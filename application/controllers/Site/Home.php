<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	    //$this->output->enable_profiler(TRUE);
	}
	public function index()
	{
		$data['title']= "Beauty Agatha"; 
		$this->load->vars($data);
		$this->load->view('studiosite/home');


	}
}
